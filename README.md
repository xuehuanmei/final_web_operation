# 网站运营期末项目报告
| 网站名称  | 行业视野                  |
|-------|-----------------------|
| 网站url | https://www.xhmzh.top |
| 网站副标题 | 设计行业从这里开始 |
| 站长    | 薛焕媚                   |
| 站长学号 | 181013005 |

## 一、策划文档
### 1.1策划和网站地图：
为不同阶段、不同类型的设计师提供一种学习平台，从这里了解设计的基本软件用法，或者是基本的设计规范。除此之外，还能学习大师的优秀设计作品，丛中得到启发与灵感，致力打造一个设计师觉得十分有价值的网站。
#### 目标用户画像：

| ![用户头像1](https://images.gitee.com/uploads/images/2020/0715/003452_0447909d_2228907.jpeg "用户头像1.jpg")  |  盈盈女性19岁 |
|---|---|
| 人物简介  | 目前正在自学设计相关知识，如使用 PS、AI、CDR等软件。正在培养自己的审美能力、独立完成设计工作能力。  |
| 用户行为  |  多数情况下会使用电脑设备进行设计操作，在学习期间通过浏览网站浏览教程来获取相关学习资料，  |
| 用户习惯与期望  | 喜欢鲜艳且色彩相互碰撞的风格，浏览网站的时候更加注意功能分区是否明确，操作是否简便。 |
| 目前用户痛点  |  目前网上关于设计的知识过于泛滥，质量参齐不齐，想要寻找一个能够收集到高质量高品质的设计相关知识的平台。 |
#### 使用DVF模型描述如何定义需求
![DVF模型](https://images.gitee.com/uploads/images/2020/0715/010825_386e0692_2228907.png "DVF需求模型.png")
#### 网站规划：
1. 非短期内更新性内容：确定网站风格、添加备案号、联系方式、导航栏，网站标题、副标题
2. 前期更新相关设计的文章、行业新闻、每日热点、设计规范
3. 中期添加、分享设计导航（设计素材的网站）、添加板块（在问页面）
4. 后期，从前期运营反馈回来的用户访谈内容中做统计、分析，根据用户需求解决问题，目标是：以用户为中心的设计（以人为本）
#### 网站地图：橙色字为页面，黑色字为文章
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/174743_423cf2d9_2228907.png "网站地图.png")

### 1.2平面设计插件运用
- [成果链接](https://www.xhmzh.top/index.php/industry-news/)
1. Animate it! 的两个运用
- 第一个运用在了侧边导航菜单栏，动画为“pop”,鼠标悬浮会有动画显示
![动画1](https://images.gitee.com/uploads/images/2020/0715/011512_1c0a9871_2228907.png "animateit1.png")
- 第二个运用在了侧边日历，动画为“pop”,鼠标悬浮会有动画显示
![动画2](https://images.gitee.com/uploads/images/2020/0715/011557_5f60f031_2228907.png "animate2.png")
- 第三个在底部页脚“联系方式”
2. SiteOrigin的3个页面/文章运用
- 第一个“设计导航页面”[链接](https://www.xhmzh.top/index.php/designguide-2/)
![设计导航](https://images.gitee.com/uploads/images/2020/0715/013519_cb678c1f_2228907.png "siteorigin2.png")
![设计导航成果页面](https://images.gitee.com/uploads/images/2020/0715/013535_f4ccb76e_2228907.png "siteorigin3.png")
- 第二个“行业新闻页面”[链接](https://www.xhmzh.top/index.php/industry-news/)
![行业新闻](https://images.gitee.com/uploads/images/2020/0715/013727_1e736723_2228907.png "siteorigin4.png")
![行业新闻成果页面](https://images.gitee.com/uploads/images/2020/0715/013744_c6be1a6b_2228907.png "siteorigin5.png")
- 第三个“今日热点页面”[链接](https://www.xhmzh.top/index.php/todays-hot-spots/)
![今日热点](https://images.gitee.com/uploads/images/2020/0715/014126_5b712207_2228907.png "siteorigin6.png")
![今日热点成果页面](https://images.gitee.com/uploads/images/2020/0715/014137_f0c91b85_2228907.png "siteorigin7.png")
### 1.3平面设计：
1. 网站配色
![网站配色](https://images.gitee.com/uploads/images/2020/0715/015218_41c1100d_2228907.png "网站配色.png")
- 配色风格：主打设计的神秘感-灰色、又带有设计的视觉冲突感-橙色/深橙色，文字标题还是运用了黑色，比较严肃抢眼球。
2. 对比性/可读性：板块标题主打黑色，与橙色下划线进行明显的对并，橙色下划线引导用户视觉。图片内的渐变风格的底色，选择白色标题，可读性强，与图片形成明显差距。

3. 图库版权说明：图片不是来自原创的，每篇文章最后都会进行说明来处，网站底部也有强调说明
- [图片来源链接](https://www.uisdc.com/)
- ![输入图片说明](https://images.gitee.com/uploads/images/2020/0717/024819_31301927_2228907.png "版权说明.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0717/024830_fca0e201_2228907.png "版权说明2.png")
- 图库风格/配色：图库风格属于实时风，图片会选比较新闻实时的图片（如新闻图、设计风、艺术风等），所以配色会比较杂。
- 举例：![输入图片说明](https://images.gitee.com/uploads/images/2020/0717/025131_a306d6d0_2228907.png "部分图库.png")

### 1.4三篇云端原创云端架站文章
- [云端架站流程](https://www.xhmzh.top/index.php/2020/07/13/cloud-station-erection-process/)
- [Github Education pack申请流程分享心得](https://www.xhmzh.top/index.php/2020/07/14/github-education-pack-share/)
- [不要轻易触碰wordpress这些操作！](https://www.xhmzh.top/index.php/2020/07/14/wordpress/)

## 二、网站管理
### 2.1云端架站：正常可用的云端网站，使用域名：xhmzh.top
- 加密了云端网站：https://www.xhmzh.top
### 2.2网站安全：使用Wordfence，相关监控截图
![wf1](https://images.gitee.com/uploads/images/2020/0715/121001_1e02f251_2228907.png "wf1.png")
![wf2](https://images.gitee.com/uploads/images/2020/0715/121109_958f79a8_2228907.png "屏幕截图(3).png")
![wf3](https://images.gitee.com/uploads/images/2020/0715/121141_bc61ca5f_2228907.png "屏幕截图(2).png")
- 通过wordfence的监控我了解到我的网站的安全性能还不高，只达60%。一天内就平均被攻击了6290000多次，但是这也都被wordfence阻止了。最近的安全扫描中，发现的问题都是插件更新，没什么大问题出现。其次从防火墙提供的数据中，我可以看出我网站防复杂攻击的性能很弱，警告性红标了，只达38%，如果真的有大师攻击我的网站，我会被K站的。将防火墙模式从学习模式（处于学习模式。这使Wordfence可以了解您的站点，以便我们了解如何保护它以及如何允许普通访问者通过防火墙。我们建议您在启用防火墙之前先让Wordfence学习一周。）调到 **启用和保护模式** ，启用并保护：在这种模式下，Wordfence Web应用防火墙正在主动阻止与已知攻击模式匹配的请求，并在主动保护您的站点免受攻击者的侵害。
## 2.3网站性能
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/024056_70500ca1_2228907.png "site1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/024107_25b6c82c_2228907.png "sie2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/024118_a9b96e87_2228907.png "site3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/024128_9f510d98_2228907.png "site4.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/024138_d37e1c2a_2228907.png "site5.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0715/024148_81297449_2228907.png "site6.png")
- 我在site24X7中添加了一个监视器，八个监视位置为：孟买、特拉维夫、北京、班家罗尔、利雅得、广州、河内、成都。在site24X7中，首先我可以看到八个监视位置的地方，我的网站在他们国家可用度为100%，说明没有出现什么差错，其次了解到我的网站在这八个地方的响应时间和速度，特拉维夫的响应速率最不平稳，时高时低；河内、孟买的响应速率最平稳；发现国内的响应不高，可能是因为国内的点击率不高。总体来说，网站响应速率还是要提高，维持网站的可用度，增强访问性。
### 2.4网站备份：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/170134_d978948a_2228907.png "数据备份截图.png")
## 三、网站运营
### 3.1站长工具：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0708/102002_dc467fee_2228907.png "站长.png")
![谷歌收录](https://images.gitee.com/uploads/images/2020/0716/154231_355df003_2228907.png "谷歌收录2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/154257_6119b5e7_2228907.png "谷歌收录.png")
### 百度（已收录）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/154340_20266f7a_2228907.png "百度收录1.png")
![百度流量1](https://images.gitee.com/uploads/images/2020/0716/210019_2e385d4a_2228907.png "百度流量1.png")
- 从百度的抓取频次来看，7.7日最高，因为当天我提交了sitemap让百度站长工具扫描我的网站内容，后来的几天下降了，因为内容更新的不多，差不多一天只有一篇。
### 谷歌（已收录）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/154308_bfd374e7_2228907.png "谷歌收录3.png")
### 必应（未收录有数据流量）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/213742_f41b8393_2228907.png "在这里输入图片标题")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/213752_5cf565cc_2228907.png "必应收录2.png")
- 必应站长工具有分析我提交的sitemap以及文章URL，但始终无法收录我的内容，了解文章不多，内容不足够优质，还需加强
### 3.2SEO优化（16号优化结果）
- seo关键词优化：第一张图为优化前，第二张图是优化后，总体文章的SEO为好，可读性优秀
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/213932_ce31abcb_2228907.png "seo优化2.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/214010_55f5422b_2228907.png "seo优化3.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/214058_7af3f248_2228907.png "seo优化1.png")
- seo网站链接换成英语（url
意义化：可读性较强）
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/214444_6b7addbe_2228907.png "seo方法2.png")
- 主动向搜索引擎提交文章链接
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/220112_39052fd3_2228907.png "提交URL1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/220124_00732c70_2228907.png "提交url2.png")
- 站长工具平台提交sitemap
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/220641_3bed725a_2228907.png "百度sitemap.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/220723_0bd17cc7_2228907.png "biying地图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0716/220755_53af30e7_2228907.png "谷歌地图.png")
## jecpack数据流量截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/025248_aba36f61_2228907.png "流量截图1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/025257_f4ef4b05_2228907.png "流量截图2.png")
- 分析：7.16号之前有优化过关键词等内容，但是效果不是很明显，一天最多浏览是13次。在16号那一天进行大优化，16号之前浏览数据优秀，16号之后数据回馈是7.17-7.18这几天的波动范围。从中我们可以看出浏览量有明显的提高，虽然点击量不多，但是相比往前一日之内阅读78次以已经是不错了，后续又下降了，初步分析网站的内容很少，吸引不了读者兴趣。从中我们收获了有一点点进步的点击量，并且我们主动在站长工具提交文章的url,完善文章的关键字，优化了seo,我们的在搜索引擎搜索文章的相关“关键词”，可以搜素的到我们的文章，这时也提高了我们网站的曝光率。没有得到非常好的结果，原因是文章内容、网页内容需要继续优化，并且多点宣传网站，例如在不懂媒体平台、评论区分享自己的网站。网站运营不是一两天完善的事情，需要我们继续每天的不断跟进。
### 3.3用户研究。3选2之图文展示两种用户研究方法的成果，以及如何影响了你的网站设计方案，要求将网站修改的前后方案截图对比说明。
## 方法一：用户访谈（抽出比较有价值的一条问题）
| 问：你从网站首页中，你得到了什么?没得到什么？                                                       |
|-------------------------------------------------------------------------------|
| 答：从首页中，我看到导航栏与网站标题，清楚了这个网站的定位是有关设计知识的网站，但是在今日热点中，我并没有看到标题，对我来说可读性不怎么强，甚至不想点开。 |
- 修改方案前：无标题，可读性弱
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/171934_acc79442_2228907.png "方案一前.png")
- 修改方案后：加入了标题，可读性增强
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/172021_a3e948a4_2228907.png "方案一后.png")
- 方法二：用户测试(user testing)：请用户来帮忙测试我的网站。有时用户测试用于测试一个已完成的网站，也可以用于测试改版效果，或者用于在网站发布之前发现可用性的问题。用户反馈说，内容不够多，设计导航比较少，而且分类不明显，分类内容不多
- 修改方案前：内容少，设计导航内容、分类不强
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/173005_8a3ffecb_2228907.png "siteorigin3.png")
- 修改方案后：增加多了几个设计导航，并且放入同一行中，以显示为同类设计
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/173104_358fa58c_2228907.png "改善后.png")
### 3.4定制化： 
#### 定制一：免受垃圾评论插件：Akismet可能是保护您的站点免受垃圾评论的世界上最好的方式。 您的站点已完全配置并受到不断的保护。
- 原因：网络社会评论显而易见，但是优质的评论能为网站加分，也更加吸引用户，同时也起到了保护网站的作用
![定制1](https://images.gitee.com/uploads/images/2020/0715/180749_a3ce5239_2228907.png "定制1.png")
![定制2](https://images.gitee.com/uploads/images/2020/0715/180804_bd8bd134_2228907.png "定制2.png")
#### 定制二：根据用户访谈/用户测试，反馈打开网站太慢了，百度搜索解决方案，得出使用WP Super Cache插件，加快页面加载，快速缓存插件。
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/173433_d183081e_2228907.png "2.1.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0719/173441_f4a2217b_2228907.png "2.2.png")
![定制化2](https://images.gitee.com/uploads/images/2020/0715/181251_f6c35d54_2228907.png "定制化2.png")

### 使用HTTPS
- 加密了云端网站：https://www.xhmzh.top

